@isTest
private class TemperatureConverterTest {
    
    @isTest static void testBoilingPoint() {
        Decimal celsius = TemperatureConverter.FahrenheitToCelsius(212);        
        System.assertEquals(0,celsius,'Boiling point temperature is not expected.');
    } 
    
      
}